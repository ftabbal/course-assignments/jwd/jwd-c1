package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.entity.Credential;
import com.udacity.jwdnd.course1.cloudstorage.entity.Note;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = {"/home", "/"})
public class HomeController {

    private UserService userService;
    private NoteService noteService;
    private CredentialService credentialService;
    private FileService fileService;

    public HomeController(UserService userService, NoteService noteService, CredentialService credentialService, FileService fileService) {
        this.userService = userService;
        this.noteService = noteService;
        this.credentialService = credentialService;
        this.fileService = fileService;
    }

    @GetMapping
    public String homeView(Authentication authentication, Model model) {
        try {
            User user = userService.getUser(authentication.getName());
            if (user == null) {
                return "redirect:/login";
            }

            model.addAttribute("noteList", noteService.getNotes(user));
            model.addAttribute("credentialsList", credentialService.getCredentials(user));
            model.addAttribute("fileList", fileService.getFileList(user));
            return "home";
        } catch (NullPointerException e) {
            // When restarting the server, the user becomes null, redirect to login page to avoid a 500 error
            return "redirect:/login";
        }
    }

}
