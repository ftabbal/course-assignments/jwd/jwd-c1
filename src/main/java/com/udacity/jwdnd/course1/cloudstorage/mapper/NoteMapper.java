package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.entity.Note;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import org.apache.ibatis.annotations.*;
import org.springframework.security.access.annotation.Secured;

import java.util.List;

@Mapper
public interface NoteMapper {

    @Select("SELECT * FROM NOTES WHERE userid=#{userid}")
    public List<Note> getNotes(User user);

    @Select("SELECT * FROM NOTES WHERE id=#{noteid}")
    public Note getNoteById(int noteId);

    @Insert("INSERT INTO NOTES(title, description, userid) VALUES" +
            "(#{title}, #{description}, #{userId})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void insert(Note note);

    @Update("UPDATE NOTES SET title=#{title}, description=#{description} WHERE id=#{id}")
    public void updateNote(Note note);

    @Delete("DELETE FROM NOTES WHERE id=#{noteId}")
    public void delete(int noteId);
}
