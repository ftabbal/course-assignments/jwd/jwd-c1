package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.entity.File;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import com.udacity.jwdnd.course1.cloudstorage.mapper.FileMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {
    FileMapper fileMapper;
    UserService userService;

    public FileService(FileMapper fileMapper, UserService userService) {
        this.fileMapper = fileMapper;
        this.userService = userService;
    }

    public List<File> getFileList(User user) {
        return fileMapper.getFileList(user);
    }

    public void addFile(File file) {
        fileMapper.addFile(file);
    }

    public void deleteFile(int fileId) {
        fileMapper.deleteFile(fileId);
    }

    public File getFile(int fileId, int userId) {
        File file = fileMapper.getFileFromId(fileId, userId);
        if (file == null) {
            return null;
        }

        return file;
    }

    public boolean doesFileExist(String filename, int userId) {
        return fileMapper.getExistingFileName(filename, userId) != null;
    }

}
