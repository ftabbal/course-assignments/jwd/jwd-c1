package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.entity.Credential;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CredentialMapper {

    @Select("SELECT * FROM CREDENTIALS WHERE userid=#{userId}")
    public List<Credential> getCredentials(int userId);

    @Insert("INSERT INTO CREDENTIALS(url, username, password, key, userid) VALUES " +
            "(#{url}, #{username}, #{password}, #{key}, #{userId})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void addCredential(Credential credential);

    @Update("UPDATE CREDENTIALS SET url=#{url}, username=#{username}, password=#{password} WHERE id=#{id}")
    public void updateCredential(Credential credential);

    @Select("SELECT * FROM CREDENTIALS WHERE id=#{credentialId} AND userid=#{userId}")
    public Credential getCredentialById(int credentialId, int userId);

    @Delete("DELETE FROM CREDENTIALS WHERE id=#{credentialId}")
    public void deleteCredential(int credentialId);
}
