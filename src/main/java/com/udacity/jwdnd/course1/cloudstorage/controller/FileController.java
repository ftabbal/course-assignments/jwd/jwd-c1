package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.entity.File;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequestMapping("/files")
public class FileController {
    private UserService userService;
    private FileService fileService;

    public FileController(UserService userService, FileService fileService) {
        this.userService = userService;
        this.fileService = fileService;
    }

    @GetMapping
    public String fileView(Authentication authentication, Model model) {
        try {
            User user = userService.getUser(authentication.getName());
            if (user == null) {
                return "redirect:/login";
            }
            model.addAttribute("file", new File());
            model.addAttribute("fileList", fileService.getFileList(user));
            return "files";
        } catch (NullPointerException e) {
            return "redirect:/login";
        }
    }

    @GetMapping("{fileId}")
    public ResponseEntity<byte[]> getFile(@PathVariable int fileId, Authentication authentication) {
        try {
            User user = userService.getUser(authentication.getName());
            File file = fileService.getFile(fileId, user.getUserid());

            if (file.getFilename() == "") {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            return ResponseEntity
                    .ok()
                    .contentType(MediaType.parseMediaType(file.getContentType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file.getFileData());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public String uploadFile(Authentication authentication, @RequestParam("fileUpload") MultipartFile multipartFile, Model model) throws IOException {
        if (multipartFile.isEmpty())
            return "redirect:/files?upload=error-empty-file";
        User user = userService.getUser(authentication.getName());
        if (user == null) {
            return "redirect:/login";
        }
        File file = new File();
        file.setFilename(multipartFile.getOriginalFilename());

        if (fileService.doesFileExist(file.getFilename(), user.getUserid())) {
            return "redirect:/files?upload=error-file-exists";
        }

        file.setFileSize(multipartFile.getSize());
        file.setContentType(multipartFile.getContentType());
        file.setFileData(multipartFile.getBytes());
        file.setUserId(user.getUserid());
        fileService.addFile(file);
        return "redirect:/files?upload=success";
    }

    @PostMapping("/delete/{fileId}")
    public String deleteFile(@PathVariable(name = "fileId") int fileId) {
        fileService.deleteFile(fileId);
        return "redirect:/files?delete=success";
    }
}
