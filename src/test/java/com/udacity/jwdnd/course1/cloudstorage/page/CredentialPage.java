package com.udacity.jwdnd.course1.cloudstorage.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CredentialPage extends AbstractPage {

    @FindBy(id = "nav-credentials-tab")
    WebElement credentialTab;

    // Credentials elements
    @FindBy(className = "credentials")
    List<WebElement> credentialList;

    @FindBy(id = "add-credentials-button")
    WebElement addCredentialButton;

    @FindBy(id = "credential-url")
    WebElement credentialUrlField;

    @FindBy(id = "credential-username")
    WebElement credentialUsernameField;

    @FindBy(id = "credential-password")
    WebElement credentialPasswordField;

    @FindBy(id = "save-button")
    WebElement credentialSubmitButton;

    @FindBy(id = "cancel-button")
    WebElement cancelButton;

    @FindBy(id = "edit-credential-button")
    List<WebElement> editButtonList;

    @FindBy(id = "delete-credential-button")
    List<WebElement> deleteButtonList;

    @FindBy(id = "credential-url-display")
    List<WebElement> credentialUrlList;

    @FindBy(id = "credential-username-display")
    List<WebElement> credentialUsernameList;

    @FindBy(id = "credential-password-display")
    List<WebElement> credentialPasswordList;

    public CredentialPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void fillModal(String url, String username, String password) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        credentialUrlField = wait.until(webDriver -> webDriver.findElement(By.id("credential-url")));

        credentialUrlField.clear();
        credentialUrlField.sendKeys(url);

        credentialUsernameField.clear();
        credentialUsernameField.sendKeys(username);

        credentialPasswordField = wait.until(webDriver -> webDriver.findElement(By.id("credential-password")));
        credentialPasswordField.clear();
        credentialPasswordField.sendKeys(password);
    }

    public String getDecryptedPassword(int index) {
        // Adapted from
        // https://stackoverflow.com/questions/7852287/using-selenium-web-driver-to-retrieve-value-of-a-html-input
        editButtonList.get(index).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(webDriver -> {
            credentialPasswordField = webDriver.findElement(By.id("credential-password"));
            if (credentialPasswordField != null) {
                if (credentialPasswordField.getAttribute("value") != "") {
                    return true;
                }
            }
            return false;
        });

        String password = credentialPasswordField.getAttribute("value");
        cancelButton.click();
        return password;
    }

    public void addCredentials(String url, String username, String password) {
        WebDriverWait wait = new WebDriverWait(driver, 1);
        credentialTab = wait.until(webDriver -> webDriver.findElement(By.id("nav-credentials-tab")));
        credentialTab.click();
        addCredentialButton.click();

        credentialUrlField = wait.until(webDriver -> webDriver.findElement(By.id("credential-url")));

        fillModal(url, username, password);

        credentialSubmitButton.click();
    }

    public void updateCredentials(String url, String username, String password, int index) {
        WebDriverWait wait = new WebDriverWait(driver, 1);
        credentialTab = wait.until(webDriver -> webDriver.findElement(By.id("nav-credentials-tab")));
        credentialTab.click();

        editButtonList.get(index).click();


        fillModal(url, username, password);
        credentialSubmitButton.click();

    }

    public void deleteCredentials(int index) {
        WebDriverWait wait = new WebDriverWait(driver, 1);
        deleteButtonList.get(index).click();
    }

    public String getTableCredentialUrl(int index) {
        return credentialUrlList.get(index).getText();
    }

    public String getTableCredentialUsername(int index) {
        return credentialUsernameList.get(index).getText();
    }

    public String getTableCredentialPassword(int index) {
        return credentialPasswordList.get(index).getText();
    }

    public int getNumberOfCredentials() { return credentialList.size(); }
}
