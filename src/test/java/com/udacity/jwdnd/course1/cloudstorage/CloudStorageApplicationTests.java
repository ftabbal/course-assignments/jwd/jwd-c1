package com.udacity.jwdnd.course1.cloudstorage;

import com.udacity.jwdnd.course1.cloudstorage.page.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CloudStorageApplicationTests {

	@LocalServerPort
	private int port;

	private WebDriver driver;

	@BeforeAll
	// I don't have Chrome on my computer, using Firefox instead
	static void beforeAll() {
		WebDriverManager.firefoxdriver().setup();
	}

	@BeforeEach
	public void beforeEach() {
		this.driver = new FirefoxDriver();
	}

	@AfterEach
	public void afterEach() {
		if (this.driver != null) {
			driver.quit();
		}
	}

	@Test
	public void testGetLoginPage() {
		driver.get("http://localhost:" + this.port + "/login");
		Assertions.assertEquals("Login", driver.getTitle());
	}

	@Test
	public void testGetSignupPage() {
		driver.get("http://localhost:" + this.port + "/signup");
		Assertions.assertEquals("Sign Up", driver.getTitle());
	}

	@Test
	public void testSuccessSignupRedirectToLoginWithSuccessMessage() {
		TestHelpers.signup(driver, port, "signuptest", "signuptest");
		LoginPage loginPage = new LoginPage(driver);
		Assertions.assertEquals("Login", driver.getTitle());

		// Needs to be updated if the success signup message changes
		Assertions.assertEquals("You successfully signed up!", loginPage.getSignupSuccessMessage());
	}

	@Test
	public void testNotSignedInRedirectToLogin() {
		driver.get("http://localhost:" + this.port + "/");
		Assertions.assertEquals("Login", driver.getTitle());
		driver.get("http://localhost:" + this.port + "/home");
		Assertions.assertEquals("Login", driver.getTitle());
		driver.get("http://localhost:" + this.port + "/files");
		Assertions.assertEquals("Login", driver.getTitle());
		driver.get("http://localhost:" + this.port + "/credentials");
		Assertions.assertEquals("Login", driver.getTitle());
		driver.get("http://localhost:" + this.port + "/notes");
		Assertions.assertEquals("Login", driver.getTitle());
		driver.get("http://localhost:" + this.port + "/files");
		Assertions.assertEquals("Login", driver.getTitle());

		// Test an invalid page
		driver.get("http://localhost:" + port + "/invalid");
		Assertions.assertEquals("Login", driver.getTitle());
	}

	@Test
	public void testAccessHomePageAfterLogin() {
		TestHelpers.login(driver, port, "fred", "1234");
		HomePage homePage = new HomePage(driver);
		driver.get("http://localhost:" + this.port + "/home");
		Assertions.assertEquals("SuperDuperDrive - Home", driver.getTitle());
		homePage.logout();
	}

	@Test
	public void testLoginAndLogout() {
		TestHelpers.login(driver, port, "fred", "1234");
		HomePage homePage = new HomePage(driver);
		driver.get("http://localhost:" + this.port + "/home");
		homePage.logout();
		driver.get("http://localhost:" + this.port + "/home");
		// Makes sure the user is getting redirected to the login page after disconnecting
		Assertions.assertEquals("Login", driver.getTitle());
	}
}
