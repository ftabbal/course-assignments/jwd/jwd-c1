package com.udacity.jwdnd.course1.cloudstorage.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    @FindBy(id = "inputUsername")
    WebElement usernameField;

    @FindBy(id = "inputPassword")
    WebElement passwordField;

    @FindBy(id = "loginButton")
    WebElement loginButton;

    @FindBy(id = "logoutMessage")
    WebElement logoutMessage;

    @FindBy(id = "error-msg")
    WebElement errorMessage;

    @FindBy(id = "signup-success-msg")
    WebElement signupSuccessMessage;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void login(String user, String password) {
        usernameField.sendKeys(user);
        passwordField.sendKeys(password);
        loginButton.click();
    }

    public String getErrorMessage() {
        try {
            return errorMessage.getText();
        } catch (Exception e) {
            return "";
        }
    }

    public String getSignupSuccessMessage() {
        try {
            return signupSuccessMessage.getText();
        } catch (Exception e) {
            return "";
        }
    }
}
